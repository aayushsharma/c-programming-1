/*
    JTSK-320111
    a6_p4.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

/*
    @brief This is a function that divides each item of the array by 5
    @param arr[] is the array
    @param size is the size of array
    @return does not return anything since its a void function, but it divides each item and then reassigns it the list.
*/

#include <stdio.h>
#include <stdlib.h>
void divby5(float arr[], int size){
    for (int i = 0; i<size; i++){
        arr[i] = arr[i] / 5;
    }
}

int main(){
    int how_many;
    scanf("%d", &how_many);
    float * list = (float *) malloc(sizeof(float) * how_many);
    for (int i=0;i<how_many;i++){
        scanf("%f", &list[i]);      //adding elements to the list
    }

    printf("Before:\n");
    for (int i=0; i< how_many; i++){
        printf("%.3f ", list[i]);
    }
    printf("\n");
    divby5(list, how_many);     //calling the function.
    printf("After:\n");
    for (int i=0; i< how_many; i++){
        printf("%.3f ", list[i]);
    }
    free(list);
    printf("\n");
}