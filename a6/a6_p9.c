/*
    JTSK-320111
    a6_p9.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/
#include <stdio.h>
#include <stdlib.h>

int main(){
    double a,b;
    FILE *file1, *file2;
    char temp[30];
    scanf("%s", temp);
    file1 = fopen(temp, "r");       //first file

    scanf("%s", temp);
    file2 = fopen(temp, "r");       //second file

    if (file1 == NULL || file2 == NULL){
        printf("Error\n");
        exit(1);
    }

    fscanf(file1, "%lf", &a);
    fscanf(file2, "%lf", &b);

    double sum = a +b;
    double diff = a-b;
    double pro = a*b;
    double division = a/b;

    FILE *outfile;
    outfile = fopen("results.txt", "a+");
    /*Writing into the file*/
    fprintf(outfile, "%lf", sum);
    fprintf(outfile, "%c", '\n');  //new line
    fprintf(outfile, "%lf", diff);
    fprintf(outfile, "%c", '\n');
    fprintf(outfile, "%lf", pro);
    fprintf(outfile, "%c", '\n');
    fprintf(outfile, "%lf", division);
}