/*
    JTSK-320111
    a6_p7.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include<stdio.h>

int prime(int n, int i){
    //testing for best cases
    if (n<=2){
        return (n==2);
    }
    if (n%i == 0){
        return 0;
    }
    if (i*i > n){
        return 1;
    }
    //recursive loop
    return prime(n, i+1);
}

int main(){
    int var;
    scanf("%d", &var);
    if (prime(var,2) == 1){
        printf("%d is prime", var);
    }else{
        printf("%d is not prime", var);
    }
    printf("\n");
}