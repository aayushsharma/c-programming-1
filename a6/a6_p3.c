/*
    JTSK-320111
    a6_p3.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include<stdio.h>
#include <string.h>
#include <ctype.h>
int count_lower(char *str){
    char *p;
    int lowercase=0;
    for (p=str; *p; p++){
        if(islower(p[0])){
            lowercase++;
        }
    }
    return lowercase;
}

int main(){
    char str[50];
    while(1){
        fgets(str, sizeof(str), stdin);
        if(str[0] == '\n'){
            break;
        }
        printf("The number of lowercase characters: %d\n", count_lower(str));
    }

}