/*
    JTSK-320111
    a6_p8.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    char ch;
    FILE *finput;
    finput = fopen("chars.txt", "r");
    if (finput == NULL){
        printf("Some error occoured!\n");
        exit(1);
    }
    int sum=0;
    for (int i=0; i < 2; i++){
        ch = getc(finput);          //read from the file
        sum = sum + ch;             //calculate sum
    }
    fclose(finput);

    FILE *foutput;
    foutput = fopen("codesum.txt", "w");
    if (foutput == NULL){
        printf("Some error occoured!\n");
    }
    fprintf(foutput, "%d", sum);        //write into file
    fclose(foutput);
    return 0;
}

