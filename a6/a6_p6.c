/*
    JTSK-320111
    a6_p6.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include<stdio.h>

int recursion(int n){
    if (n==1){
        printf("%d", n);
        return 1;
    }else{
        printf("%d, ", n);
        return recursion(n-1);
    }
}

int main(){
    int var;
    scanf("%d", &var);
    recursion(var);
    printf("\n");
}