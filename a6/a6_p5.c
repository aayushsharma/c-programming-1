/*
    JTSK-320111
    a6_p5.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/
#include <stdio.h>
#include <float.h>

//returns the scalar function
double scalar(double v[], double w[], int n){
    double mult=1,sum=0;
    for(int i = 0; i<n; i++){
        mult = v[i] * w[i];
        sum += mult;
    }
    return sum;
}

//return largest element and its index
void large(double list[], int n){
    double largest = -DBL_MAX;
    int index;
    for (int i=0;i<n;i++){
        if (list[i] > largest){
            largest = list[i];
            index=i;
        }
    }
    printf("The largest = %lf\n", largest);
    printf("Position of largest = %d\n", index);
    return;
}

//return smallest element and its index
void small(double list[], int n){
    double smallest = DBL_MAX;
    int index=0;
    for (int j=0;j<n;j++){
        if (list[j] < smallest){
            smallest = list[j];
            index = j;
        }
    }
    printf("The smallest = %lf\n", smallest);
    printf("Position of smallest = %d\n", index);
    return;
}

//main function
int main(){
    int n;
    double v[100], w[100];
    scanf("%d", &n);
    for (int i=0; i<n;i++){
        scanf("%lf",&v[i]);
    }

    for (int i=0; i<n;i++){
        scanf("%lf",&w[i]);
    }

    printf("Scalar product=%lf\n", scalar(v,w,n));      //printed from main function
    small(v,n);         //not printed from main function because question did not specify it.
    large(v,n);
    small(w,n);
    large(w,n);

}