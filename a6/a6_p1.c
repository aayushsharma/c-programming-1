/*
    JTSK-320111
    a6_p1.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <string.h>
/*
    @brief This is a function that return the list of triangles
    @param n is the base and height of the triangle
    @param ch is the character the traingle must be filled with
    @return does not return anything since its a void function, but it does print the triangle.
*/
void triangle(int n, char ch){
    int tempn = n;
    for (int i = 1; i<=tempn; i++){
        for (int j = 0; j<n; j++){
            printf("%c", ch);
        }
        printf("\n");
        n--;
    }
}
int main(){
    int n;
    char ch;
    char tempholder[10];
    fgets(tempholder, sizeof(tempholder), stdin);
    sscanf(tempholder, "%d", &n);
    fgets(tempholder, sizeof(tempholder), stdin);
    sscanf(tempholder, "%c", &ch);
    triangle(n,ch);
}
