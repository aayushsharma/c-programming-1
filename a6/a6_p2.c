/*
    JTSK-320111
    a6_p2.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

/*
    @brief This is a function that divides each item of the array by 5
    @param arr[] is the array
    @param size is the size of array
    @return does not return anything since its a void function, but it divides each item and then reassigns it the table.
*/

#include <stdio.h>
void divby5(float arr[], int size){
    for (int i = 0; i<size; i++){
        arr[i] = arr[i] / 5;
    }
}

int main(){
    float list[6] = {5.5,6.5,7.75,8.0,9.6,10.36};
    printf("Before:\n");
    for (int i=0; i< 6; i++){
        printf("%.3f ", list[i]);
    }
    printf("\n");
    divby5(list, 6);
    printf("After:\n");
    for (int i=0; i< 6; i++){
        printf("%.3f ", list[i]);
    }
    printf("\n");

}