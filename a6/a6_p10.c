/*
    JTSK-320111
    a6_p10.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(){
    FILE *file1, *file2;
    char input1[50], input2[50];
    file1 = fopen("text1.txt", "r");
    file2 = fopen("text2.txt", "r");

    //adding contents of file1 to input1
    int i=0;
    while (1){
        input1[i] = getc(file1);
        if (input1[i] == EOF){
            break;
        }
        i++;
    }
    input1[i-1] = '\0';  //end of string

    //adding contents of file2 to input2
    int j =0;
    while (1){
        input2[j] = getc(file2);
        if (input2[j] == EOF){
            break;
        }
        j++;
    }
    input2[j-1] = '\0'; //end of string

    //merging two strings together
    char new_str[100];
    strcat(new_str, input1);
    strcat(new_str, input2);

    //writing into the file
    FILE * outfile;
    outfile = fopen("merge12.txt", "w");
    fprintf(outfile, "%s", new_str);

    printf("Merged!\n");



}
