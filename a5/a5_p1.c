#include <stdio.h>
#include <math.h>
double const PI = 3.141592653589793238462;

int main(){
    float ul,ll,step;
    scanf("%f", &ul);
    scanf("%f", &ll);
    scanf("%f", &step);
    int stepforLoop = (ll-ul) / step;
    for (int i = 0; i<=stepforLoop; i++){
        printf("%f %f %f\n", ul, ul*ul*PI, 2*PI*ul);
        ul = ul + step;
    }
}