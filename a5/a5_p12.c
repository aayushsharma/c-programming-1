/*
    JTSK-320111
    a5_p12.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

void replaceAll(char *str, char c, char e){
    for (int i = 0; i < strlen(str); i++){
        if (str[i] == c){
            str[i] = e;
        }
    }
    return;
}

int main(){
    char array[80];
    char e,c;
    for (int i = 0; i<80;i++){
        printf("Enter a string to manipulate: ");
        fgets(array, sizeof(array), stdin);
        array[strlen(array)]='\0';
        /*break loop if stop is entered*/
        if (tolower(array[0]) == 's' && tolower(array[1]) == 't' && tolower(array[2]) == 'o' && tolower(array[3]) == 'p' ){
            break;
        }
        printf("Enter a character to replace: ");
        scanf("%c", &c);
        getchar();
        printf("Enter a character to replace that with: ");
        scanf("%c", &e);
        getchar();
        printf("\nBefore replacement: %s\n", array);
        /*Call the function*/
        replaceAll(array,c,e);
        printf("After replacement: %s\n", array);

    }
}

