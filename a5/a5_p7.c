/*
    JTSK-320111
    a5_p7.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>

void underTheMainDiagonal(int grid[][30], int dim){
    printf("Under the main diagonal:\n");
    for (int row=0; row<dim; row++){
        for(int col=0; col<dim; col++){
            if (col < row){
                printf("%d ", grid[row][col]);
            }
        }
    }
    printf("\n");
    return;
}

void printMatrix(int grid[][30], int dim){
    printf("The entered matrix is:\n");
    for (int row=0; row<dim; row++){
        for(int col=0; col<dim; col++){
            printf("%d ", grid[row][col]);
        }
        printf("\n");
    }
    return;
}

int main(){
    int rowCol;
    scanf("%d", &rowCol);
    int matrix[30][30];
    for (int row=0; row<rowCol; row++){
        for(int col=0; col<rowCol; col++){
            scanf("%d", &matrix[row][col]);
        }
    }

    printMatrix(matrix,rowCol);
    underTheMainDiagonal(matrix, rowCol);
    return 0;
}
