/*
    JTSK-320111
    a5_p9.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>

int prodminmax(int arr[], int n){
    int max = INT_MIN;
    int min = INT_MAX;
    for (int i=0; i< n; i++){
        if (arr[i] > max){
            max = arr[i];
        }
        if (arr[i] < min){
            min = arr[i];
        }
    }
    return max*min;
}

int main(){
    // int arr[10] = {2,2,3,4,5,6,7,8,90,20};
    int n;
    printf("How many?: ");
    scanf("%d", &n);
    int * array = (int *) malloc(sizeof(int) * n);
    for (int i=0;i<n;i++){
        printf("Enter element: ");
        scanf("%d", &array[i]);
    }

    printf("Product of min and max elements : %d", prodminmax(array, n));
    free(array);
}