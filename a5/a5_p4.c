/*
    JTSK-320111
    a5_p4.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <string.h>

// Function for counting consonants
int count_consonants(char str[]){
    int consonants = 0;
    for (int i=0;i<strlen(str); i++){
        if ( ((str[i]>='a') && (str[i]<='z')) || ((str[i]>='A') && (str[i]<='Z'))){     //check if alphabet or not.
            if((str[i]!='A') && (str[i]!='E') && (str[i]!='I') && (str[i]!='O') && (str[i]!='U') && (str[i]!='a') && (str[i]!='e') && (str[i]!='i') && (str[i]!='o') && (str[i]!='u')){ //check if vowel or not
                consonants++;
            }
        }
    }
    return consonants;
}
int main(){
    char input[100];
    while (1){
        fgets(input, sizeof(input), stdin);
        if(input[0] == '\n'){
            break;
        }
        printf("Number of consonants=%d\n", count_consonants(input));    //Printing the count_consonants
    }
    return 0;
}