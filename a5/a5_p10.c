/*
    JTSK-320111
    a5_p10.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <math.h>
void proddivpowinv(float a, float b, float *prod, float *div,float *pwr, float *invb){
    * prod = a * b;
    * div = a / b;
    * pwr = pow(a,b);
    * invb = 1/b;
    return;
}

int main(){
    float a,b, prod, div, pwr, invb;
    scanf("%f", &a);
    scanf("%f", &b);
    proddivpowinv(a,b,&prod,&div,&pwr,&invb);   //calling the void function.
    printf("Product: %f\n", prod);              //Product
    printf("Division: %f\n", div);              //Division
    printf("Power: %f\n", pwr);                 //Power a to b
    printf("Inverse: %f\n", invb);              //Inverse of b
}