/*
    JTSK-320111
    a5_p8.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>

void printMatrix(int grid[][30], int dim){
    printf("The entered matrix is:\n");
    for (int row=0; row<dim; row++){
        for(int col=0; col<dim; col++){
            printf("%d ", grid[row][col]);
        }
        printf("\n");
    }
    return;
}

void underSecondary(int grid[][30], int dim){
    printf("Under the secondary diagonal:\n");
    for (int row=0;row<dim;row++){
        for(int col=0;col<dim;col++){
            if((row+col >= dim) && (row+col < (2*dim-1))){
                printf("%d ", grid[row][col]);
            }
        }
    }
    printf("\n");
}

int main(){
    int rowCol;
    scanf("%d", &rowCol);
    int matrix[30][30];
    for (int row=0; row<rowCol; row++){
        for(int col=0; col<rowCol; col++){
            scanf("%d", &matrix[row][col]);
        }
    }
    printMatrix(matrix,rowCol);
    underSecondary(matrix,rowCol);
}
