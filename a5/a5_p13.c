/*
    JTSK-320111
    a5_p13.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

//change to uppercase function.

void uppcase(char * str){
    for (int i=0;i<strlen(str); i++){
        if (islower(str[i])){
            str[i] = toupper(str[i]);
        }
    }
    return;
}

//change to lowercase function.
void lowcase(char * str){
    for (int i=0;i<strlen(str); i++){
        if (isupper(str[i])){
            str[i] = tolower(str[i]);
        }
    }
    return;
}

int main(){
    char test[90];
    do{
        fgets(test, sizeof(test), stdin);
        if (test[0] == 'e' && test[1] == 'x' && test[2] == 'i' && test[3] == 't'){
            break;
        }
        uppcase(test); //calling uppercase function
        printf("uppcase=%s", test);
        lowcase(test);  //calling lowercase function
        printf("lowcase=%s", test);

    }while (1);

}