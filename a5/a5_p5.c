/*
    JTSK-320111
    a5_p5.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <string.h>

int count_consonants(char str[]){
    char *p;
    int consonants = 0;
    for (p = str; *p;p++){
        if ( ((p[0]>='a') && (p[0]<='z')) || ((p[0]>='A') && (p[0]<='Z'))){     //check if alphabet or not.
            if((p[0]!='A') && (p[0]!='E') && (p[0]!='I') && (p[0]!='O') && (p[0]!='U') && (p[0]!='a') && (p[0]!='e') && (p[0]!='i') && (p[0]!='o') && (p[0]!='u')){ //check if vowel or not
                consonants++;
            }
        }
    }

    return consonants;
}


int main(){
    char input[100];
    while (1){
        fgets(input, sizeof(input), stdin);
        if(input[0] == '\n'){
            break;
        }
        printf("Number of consonants=%d\n", count_consonants(input));    //Printing the count_consonants
    }
    return 0;
}