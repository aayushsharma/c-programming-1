/*
    JTSK-320111
    a5_p3.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/
#include <stdio.h>
#include <math.h>

/* Function for Geometric mean */
float geometric_mean(float arr[], float num ){
    float gm=1;
    for (int index=0; index < num; index++){
        gm = gm * (powf(arr[index], 1/(num)));
    }
    return gm;
}

/*Function for sum of all elements of the list */
float sum(float list[], float num){
    float total=0;
    for (int i =0; i<num; i++){
        total = total + list[i];
    }
    return total;
}

/*Function to return the highest element of the list */
float high(float list[], float num){
    float big = 0;
    for (int i = 0; i< num; i++){
        if (list[i] > big){
            big = list[i];
        }
    }
    return big;
}

/*Function to return the lowest element of the list */
float low(float list[], float num){
    float small = 99999999999;
    for (int i = 0; i< num; i++){
        if (list[i] < small){
            small = list[i];
        }
    }
    return small;
}

/* Main Function */
int main(){
    float list[15];
    int size=0;
    for (int i=0; i<15; i++){
        float tempvalue;
        char templine[15];
        fgets(templine,sizeof(templine), stdin);   //input the values into the array
        sscanf(templine,"%f", &tempvalue);
        if (tempvalue < 0){                        //check if number if greater than 0 or not. If not, break the loop.
            break;
        }else{
            size++;
            list[i] = tempvalue;
        }
    }
    char decide;
    char line[10];
    printf("Enter decide\n");
    fgets(line, sizeof(line), stdin);
    sscanf(line,"%c", &decide);

    /*What does the user want to do */
    switch (decide){
        case 'm':
            printf("Geometric Mean: %f\n", geometric_mean(list, size));     //Prints geometric mean

        case 's':
            printf("Sum: %f\n", sum(list, size));                //Prints sum

        case 'h':
            printf("Highest: %f", high(list, size));            //Prints highest element

        case 'l':
            printf("Lowest: %f", low(list, size));              //Prints the lowest element
    }
}