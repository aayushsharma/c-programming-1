/*
    JTSK-320111
    a5_p6.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <stdlib.h>
int main(){
    int *dyn_array, how_many, i;
    printf("How many elements? ");
    scanf("%d", &how_many);
    dyn_array = (int *) malloc(sizeof(int) * how_many);
    if (dyn_array == NULL){
        exit(1);
    }
    for (i = 0; i < how_many; i++){
        printf("\n Input number %d: ", i);
        scanf("%d", &dyn_array[i]);
    }

    int great1 = 0;
    int great2 = 0;
    for (i = 0; i < how_many; i++){
        /* If current element is greater than great1 then update both great1 and great2*/
        if (dyn_array[i] > great1){
            great2 = great1;                                //push the previous larger value to great2
            great1 = dyn_array[i];                          //assign the now greater value to great1
        /* If the current element is in between great1 and great2 assign it to great2*/
        }else if (dyn_array[i] > great2 && dyn_array[i] != great1){
            great2 = dyn_array[i];
        }
    }
    printf("First greatest number %d\n", great1);
    printf("Second greatest number %d\n", great2);
    free(dyn_array);
    return 0;
}