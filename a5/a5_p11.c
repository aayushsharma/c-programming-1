/*
    JTSK-320111
    a5_p11.c
    Aayush Sharma Acharya
    a.sharmaacharya@jacobs-university.de
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int count_insensitive(char * str, char c){
    int lettercount=0;
    for (int i=0; i<strlen(str); i++){
        if (tolower(str[i]) == tolower(c)){                      //check for characters
            lettercount++;
        }
    }
    return lettercount;
}

int main(){
    char * tempString;
    tempString = (char *) malloc(sizeof(char) * 50);            //Temporary String that we are going to copy from
    if (tempString == NULL){
        exit(1);
    }
    fgets(tempString, (sizeof(char) * 50), stdin);
    int length = strlen(tempString);

    char string[length];                                        //Real String that we are going to copy to.
    for (int i=0; i<length; i++){
        string[i] = tempString[i];                              //Copying
    }
    free(tempString);                                           //Freeing the memory.

    string[length] = '\0';
    printf("The character 'b' occours: %d times.\n", count_insensitive(string, 'b'));
    printf("The character 'H' occours: %d times.\n", count_insensitive(string, 'H'));
    printf("The character '8' occours: %d times.\n", count_insensitive(string, '8'));
    printf("The character 'u' occours: %d times.\n", count_insensitive(string, 'u'));
    printf("The character '$' occours: %d times.\n", count_insensitive(string, '$'));
}