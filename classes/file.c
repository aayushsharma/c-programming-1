#include <stdio.h>
#include <stdlib.h>
int main(){
    char ch;
    FILE * fp1, * fp2;
    fp1 = fopen("src.txt", "r");
    if (fp1 == NULL){
        printf("Cannot open file!\n");
        exit(1);
    }

    fp2 = fopen("dest.txt", "w");
    if (fp1 == NULL){
        printf("Cannot open file!\n");
        exit(1);
    }


    while((ch=getc(fp1))!=EOF){
        putc(ch, fp2);
    }

    fclose(fp1);
    fclose(fp2);
    return 0;
}