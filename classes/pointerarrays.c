#include <stdio.h>


int main(){
    int *dyn_array, how_many, i;

    //Read the number of elements from users
    printf("how many elements?");
    scanf("%d", &how_many);

    //Dynamically allocate memory for the array
    dyn_array = (int*) malloc(sizeof(int) * how_many);
    if (dyn_array == NULL){
        exit(1); //same as return 1; in main function
    }

    //Get the element and write it to array
    for (int i; i<how_many; i++){
        printf("\nInput number %d:", i);
        scanf("%d", &dyn_array[i]);
    }

}