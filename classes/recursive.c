#include <stdio.h>

int factorial (int n){
    int val;
    if ((n == 0) || (n == 1)){
        return 1;
    }else {
        val = factorial(n-2) + factorial(n-1);
        return val;
    }
}

int main(){
    printf("%d\n", factorial(10));
    return 0;
}